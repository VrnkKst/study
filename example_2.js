
const Brandcar ='Toyota';
const ModelName = 'Camry';
const isAdmin = false; 
const Guarantee = null;
const DateManufactured = undefined;
const Restaling = 'restaling.2';
const Age = 12;

//Строковое преобразование
const resultStringBrandcar = String(Brandcar);
const resultStringisAdmin = String(isAdmin); 
const resultStringGuarantee = String(Guarantee);
const resultStringDateManufactured = String(DateManufactured);
const resultStringAge = String(Age);
//console.log({ resultStringBrandcar, resultStringisAdmin, resultStringGuarantee, resultStringDateManufactured, resultStringAge })

//Числовое преобразование
const resultNumberBrandcar = Number(Brandcar);
const resultNumberisAdmin = Number(isAdmin); 
const resultNumberGuarantee = Number(Guarantee);
const resultNumberDateManufactured = Number(DateManufactured);
const resultNumberAge = Number(Age);
//console.log({ resultNumberBrandcar, resultNumberisAdmin, resultNumberGuarantee, resultNumberDateManufactured, resultNumberAge })


//Логическое преобразование
const resultBooleanBrandcar = Boolean(Brandcar);
const resultBooleanisAdmin = Boolean(isAdmin); 
const resultBooleanGuarantee = Boolean(Guarantee);
const resultBooleanDateManufactured = Boolean(DateManufactured);
const resultBooleanAge = Boolean(Age);
//console.log({ resultBooleanBrandcar, resultBooleanisAdmin, resultBooleanGuarantee, resultBooleanDateManufactured, resultBooleanAge })

const resultisAdminAndCash = '000' == true;

console.log({ resultisAdminAndCash})